package ke.co.virtuaeye.squziebiz.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.fivehundredpx.android.blur.BlurringView;

import ke.co.virtuaeye.squziebiz.R;
import ke.co.virtuaeye.squziebiz.utils.DefaultImageLoader;

/**
 * Created by Frederick on 10/26/2015.
 */
public class DatingFragment extends Fragment {
    private GridView gridView;
    private BlurringView blurringView;
    private ImageView largerImage;
    private View imageHolder;


    public DatingFragment() {
    }

    public static Fragment newInstance() {
        return new DatingFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.dating_fragment, container, false);
        blurringView = (BlurringView) parent.findViewById(R.id.blurring_view);
        gridView = (GridView) parent.findViewById(R.id.gridView);
        largerImage = (ImageView) parent.findViewById(R.id.imageView7);
        imageHolder = parent.findViewById(R.id.fullSize);

        blurringView.setBlurredView(gridView);
        final GalleryAdapter adapter = new GalleryAdapter(inflater, getImages());
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setClickable(false);
                blurringView.setVisibility(View.VISIBLE);
                imageHolder.setVisibility(View.VISIBLE);
                DefaultImageLoader.getInstance().loadImage("drawable://" + (int) adapter.getItem(position), largerImage);
            }
        });
        return parent;
    }

    private class GalleryAdapter extends BaseAdapter {
        LayoutInflater layoutInflater;

        //dummy, real image urls will replace this data
        int[] images;

        public GalleryAdapter(LayoutInflater layoutInflater, int[] images) {
            this.layoutInflater = layoutInflater;
            this.images = images;
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int position) {
            return images[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.list_item_dating_gallery, null);
                convertView.setTag(holder);
                holder.profilePic = (ImageView) convertView.findViewById(R.id.imageView);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            DefaultImageLoader.getInstance().loadImage("drawable://" + images[position], holder.profilePic);
            Log.d("boom", "loaded - " + position);
            return convertView;
        }

        private class ViewHolder {
            ImageView profilePic;
        }
    }

    //some image placeholders
    private int[] getImages() {
        return new int[]{
                R.drawable.a1dcqx,
                R.drawable.a44ewla,
                R.drawable.a31p3w3,
                R.drawable.zzx4wzs,
                R.drawable.b3dvlt,
                R.drawable.bomwinp,
                R.drawable.cstbdo2,
                R.drawable.a9wlrfq,
                R.drawable.a5v0dte,
                R.drawable.bokczvd,
                R.drawable.gpxlwt7,
                R.drawable.hlkwj24,
                R.drawable.hfag385,
                R.drawable.s5n5ryd
        };
    }
}

