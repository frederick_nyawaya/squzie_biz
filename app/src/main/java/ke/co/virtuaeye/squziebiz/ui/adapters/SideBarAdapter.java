package ke.co.virtuaeye.squziebiz.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ke.co.virtuaeye.squziebiz.R;
import ke.co.virtuaeye.squziebiz.entities.NavItem;
import ke.co.virtuaeye.squziebiz.utils.DefaultImageLoader;

/**
 * Created by Frederick on 10/1/2015.
 */
public class SideBarAdapter extends BaseAdapter {
    private List<NavItem> items;
    private LayoutInflater inflater;
    private Context context;


    public SideBarAdapter(Context context, List<NavItem> items) {
        this.context = context;
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public void setItems(List<NavItem> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public NavItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();


        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_navigation_drawer, null);
            convertView.setTag(holder);
            holder.title = (TextView) convertView.findViewById(R.id.textView3);
            holder.icon = (ImageView) convertView.findViewById(R.id.imageView2);


            //Utils.applyFonts(convertView, App.getRobotoSlabLight());

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(getItem(position).getTitle());
        DefaultImageLoader.getInstance().loadImage("drawable://" + getItem(position).getDrawable(), holder.icon);

        return convertView;
    }


    private class ViewHolder {
        TextView title;
        ImageView icon;
    }
}
