package ke.co.virtuaeye.squziebiz.entities;

/**
 * Created by Frederick on 10/1/2015.
 */
public class NavItem {

    private String title;
    private int drawable;

    public NavItem(String title, int drawable) {
        this.title = title;
        this.drawable = drawable;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }
}
