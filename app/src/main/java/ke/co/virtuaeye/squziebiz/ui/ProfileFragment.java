package ke.co.virtuaeye.squziebiz.ui;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ke.co.virtuaeye.squziebiz.R;
import ke.co.virtuaeye.squziebiz.utils.DefaultImageLoader;

public class ProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private AboutMeAdapter adapter;
    private ListView list;


    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();

        return fragment;
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_profile, container, false);

        list = (ListView) parent.findViewById(R.id.listView);
        list.setAdapter(new AboutMeAdapter(inflater, getAboutMe()));
        return parent;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private class AboutMeAdapter extends BaseAdapter {
        LayoutInflater layoutInflater;


        List<AboutMe> items;

        public AboutMeAdapter(LayoutInflater layoutInflater, List<AboutMe> items) {
            this.layoutInflater = layoutInflater;
            this.items = items;
        }


        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public AboutMe getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = new ViewHolder();

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.list_item_about_me, null);
                convertView.setTag(holder);
                holder.imageView = (ImageView) convertView.findViewById(R.id.imageView3);
                holder.info = (TextView) convertView.findViewById(R.id.textView4);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            DefaultImageLoader.getInstance().loadImage("drawable://" + getItem(position).drawable, holder.imageView);
            holder.info.setText(getItem(position).text);
            Log.d("boom", "loaded - " + position);
            return convertView;
        }

        private class ViewHolder {
            ImageView imageView;
            TextView info;
        }
    }

    private class AboutMe {
        int drawable;
        String text;

        public AboutMe(int drawable, String text) {
            this.drawable = drawable;
            this.text = text;
        }
    }

    private List<AboutMe> getAboutMe() {
        List<AboutMe> aboutMes = new ArrayList<>();
        aboutMes.add(new AboutMe(R.drawable.ic_action_search,
                "Looking for nice people\nCancer\nJust a speck, in a planet that's a " +
                        "speck, in a solar system that's tinnier than a speck."));
        aboutMes.add(new AboutMe(R.drawable.ic_social_person, "Lizzy Caplan\n+1258741" +
                "\nyou@me.com\nbusy\nwhat is the meaning of life?"));
        aboutMes.add(new AboutMe(R.drawable.ic_action_work, "Company X\nChairman"));

        return aboutMes;


    }

}
